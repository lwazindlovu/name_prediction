# import libraries
import pandas as pd
import numpy as np
from sklearn import preprocessing
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import cross_val_score

# ======================= Reading data
data = pd.read_excel('./name_prediction/data.xlsx')

# ======================= Viewing Sample Balance
data['Display Name'].value_counts() # DATA SEVERELY UNBALANCED, WITH A 523:17 RATIO (0:1)

# ======================= START OF DATA CLEANING

# ======================= Train Data
# ======================= Manually balancing by selecting 1) random samples from 'Data' Dataframe
one = data.loc[data['Display Name'] == 1]   # 1
zero = data.loc[data['Display Name'] == 0]

for i in range(100):
    one = one.sample(frac=1)
    zero = zero.sample(frac=1)

one = one.sample(frac=.8)
zero = zero.head(len(data.loc[data['Display Name'] == 1]))
zero = zero.sample(frac=.8)

len(one) == len(zero) # DATA BALANCED, WITH A 1:1 RATIO (0:1)

# ======================= 1) Creating one full DataFrame, 2) randomizing row positions, 3) dropping used rows from 'Data' Dataframe

new_set = one.append(zero)          # 1

for i in range(100):                # 2
    new_set = new_set.sample(frac=1)

data = data.drop(new_set.index)     # 3

new_set.index = range(len(new_set))

# ======================= 1) Creating new DataFrame.. 2) Keeping important data, 3) curating data from other Dataframes, 4) declaring reference variables, 5) adding value to NaN cells
full_set = pd.DataFrame()       # 1
full_set['y'] = new_set['Display Name']     # 2
full_set['name'] = new_set['name']
full_set['len'] = new_set['name'].apply(len)        # 3

full_set['vowels'], full_set['consonants'], full_set['exotics'], full_set['numbers'], full_set['spaces']  = np.nan, np.nan, np.nan, np.nan, np.nan
# for i in range(full_set['len'].max()):
#     full_set['alpha_'+str(i)] = np.nan

numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']        # 4
vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x', 'z', 'w', 'y', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'X', 'Z', 'W', 'Y']
spaces = [' ']


for i in full_set.index:        # 5

    for j in range(full_set['len'][i]):

        if full_set['name'][i][j] in numbers:           # Numbers

            if np.isnan(full_set['numbers'][i]) == True:
                full_set['numbers'][i] = 1
            else:
                full_set['numbers'][i] +=1

        elif full_set['name'][i][j] in spaces:              # Spaces

            if np.isnan(full_set['spaces'][i]) == True:
                full_set['spaces'][i] = 1
            else:
                full_set['spaces'][i] +=1

        elif full_set['name'][i][j] in vowels:              # Vowels

            if np.isnan(full_set['vowels'][i]) == True:
                full_set['vowels'][i] = 1
            else:
                full_set['vowels'][i] +=1

        elif full_set['name'][i][j] in consonants:              # Consonants
            if np.isnan(full_set['consonants'][i]) == True:
                full_set['consonants'][i] = 1
            else:
                full_set['consonants'][i] +=1

        else:                                                      # Exotics
            if np.isnan(full_set['exotics'][i]) == True:
                full_set['exotics'][i] = 1
            else:
                full_set['exotics'][i] +=1

        # full_set['alpha_'+str(j)][i] = full_set['name'][i][j]


x_train = full_set[full_set.columns[2:8]]
y_train = full_set[full_set.columns[:1]]

x_train = pd.DataFrame(columns=x_train.columns, data=LabelEncoder().fit_transform(x_train.values.flatten()).reshape(x_train.shape))

# ======================= Viewing Sample Balance
data['Display Name'].value_counts() # DATA SEVERELY UNBALANCED, WITH A 151:1 RATIO (0:1)

# ======================= Test Data
# ======================= Manually balancing by selecting 1) random samples from 'Data' Dataframe
one = data.loc[data['Display Name'] == 1]   # 1
zero = data.loc[data['Display Name'] == 0]

for i in range(100):
    one = one.sample(frac=1)
    zero = zero.sample(frac=1)

one = one.sample(frac=1)
zero = zero.head(len(data.loc[data['Display Name'] == 1]))
zero = zero.sample(frac=1)

len(one) == len(zero) # DATA BALANCED, WITH A 1:1 RATIO (0:1)

# ======================= 1) Creating one full DataFrame, 2) randomizing row positions, 3) dropping used rows from 'Data' Dataframe

new_set = one.append(zero)          # 1

for i in range(100):                # 2
    new_set = new_set.sample(frac=1)

data = data.drop(new_set.index)     # 3

new_set.index = range(len(new_set))

# ======================= 1) Creating new DataFrame.. 2) Keeping important data, 3) curating data from other Dataframes, 4) declaring reference variables, 5) adding value to NaN cells
full_set = pd.DataFrame()       # 1
full_set['y'] = new_set['Display Name']     # 2
full_set['name'] = new_set['name']
full_set['len'] = new_set['name'].apply(len)        # 3

full_set['vowels'], full_set['consonants'], full_set['exotics'], full_set['numbers'], full_set['spaces']  = np.nan, np.nan, np.nan, np.nan, np.nan
# for i in range(full_set['len'].max()):
#     full_set['alpha_'+str(i)] = np.nan

for i in full_set.index:        # 5

    for j in range(full_set['len'][i]):

        if full_set['name'][i][j] in numbers:           # Numbers

            if np.isnan(full_set['numbers'][i]) == True:
                full_set['numbers'][i] = 1
            else:
                full_set['numbers'][i] +=1

        elif full_set['name'][i][j] in spaces:              # Spaces

            if np.isnan(full_set['spaces'][i]) == True:
                full_set['spaces'][i] = 1
            else:
                full_set['spaces'][i] +=1

        elif full_set['name'][i][j] in vowels:              # Vowels

            if np.isnan(full_set['vowels'][i]) == True:
                full_set['vowels'][i] = 1
            else:
                full_set['vowels'][i] +=1

        elif full_set['name'][i][j] in consonants:              # Consonants
            if np.isnan(full_set['consonants'][i]) == True:
                full_set['consonants'][i] = 1
            else:
                full_set['consonants'][i] +=1

        else:                                                      # Exotics
            if np.isnan(full_set['exotics'][i]) == True:
                full_set['exotics'][i] = 1
            else:
                full_set['exotics'][i] +=1

        # full_set['alpha_'+str(j)][i] = full_set['name'][i][j]


x_test = full_set[full_set.columns[2:8]]
y_test = full_set[full_set.columns[:1]]

x_test = pd.DataFrame(columns=x_test.columns, data=LabelEncoder().fit_transform(x_test.values.flatten()).reshape(x_test.shape))

# ======================= END OF DATA CLEANING

# ======================= START OF PREDICTION MODELING
# ======================= Hot-One Encoding
le = preprocessing.LabelEncoder()

# ======================= Training the model
clf = DecisionTreeClassifier(random_state=0)
cross_val_score(clf, x_train, y_train, cv=10)

clf.fit(x_train, y_train)

# ======================= Testing the model
clf.predict(x_test)

clf.score(x_test, y_test)
